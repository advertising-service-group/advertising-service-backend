package ru.adservice.backend.services;

import static org.junit.Assert.*;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import ru.adservice.backend.Main;
import ru.adservice.backend.dto.CategoryDTO;
import ru.adservice.backend.exceptions.*;
import java.util.List;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest(classes = Main.class)
@WebAppConfiguration
public class DefaultCategoriesServiceTest {
    @Autowired
    private DefaultCategoriesService service;

    @Test
    public void saveValid() throws NullParamException, NullDTOException, ItemsAssociatedException {
        try {
            CategoryDTO testDTO = new CategoryDTO();
            testDTO.setName("TestCategoryName");
            testDTO.setReqName("TestCategoryReqName");
            testDTO.setDeleted(false);
            CategoryDTO saved = service.saveCategory(testDTO);
            assertEquals(testDTO.getName(), saved.getName());
            assertEquals(testDTO.getReqName(), saved.getReqName());
            assertEquals(testDTO.getDeleted(), saved.getDeleted());
        } catch (ValidationException ve) {
            // test category already exists -> re-saving
            delete();
            saveValid();
        }
    }

    @Test(expected = NullDTOException.class)
    public void saveNullDTO() throws NullParamException, NullDTOException, ValidationException {
        service.saveCategory(null);
    }

    @Test(expected = NullParamException.class)
    public void saveNullName() throws NullParamException, NullDTOException, ValidationException {
        CategoryDTO cDTO = new CategoryDTO();
        cDTO.setId(1);
        cDTO.setName(null);
        cDTO.setReqName("ReqName");
        cDTO.setDeleted(false);
        service.saveCategory(cDTO);
    }

    @Test(expected = NullParamException.class)
    public void saveEmptyName() throws NullParamException, NullDTOException, ValidationException {
        CategoryDTO cDTO = new CategoryDTO();
        cDTO.setId(1);
        cDTO.setName("");
        cDTO.setReqName("ReqName");
        cDTO.setDeleted(false);
        service.saveCategory(cDTO);
    }

    @Test(expected = NullParamException.class)
    public void saveNullReqName() throws NullParamException, NullDTOException, ValidationException {
        CategoryDTO cDTO = new CategoryDTO();
        cDTO.setId(1);
        cDTO.setName("Name");
        cDTO.setReqName(null);
        cDTO.setDeleted(false);
        service.saveCategory(cDTO);
    }

    @Test(expected = NullParamException.class)
    public void saveEmptyReqName() throws NullParamException, NullDTOException, ValidationException {
        CategoryDTO cDTO = new CategoryDTO();
        cDTO.setId(1);
        cDTO.setName("Name");
        cDTO.setReqName("");
        cDTO.setDeleted(false);
        service.saveCategory(cDTO);
    }

    @Test(expected = NullParamException.class)
    public void saveNullDeleted() throws NullParamException, NullDTOException, ValidationException {
        CategoryDTO cDTO = new CategoryDTO();
        cDTO.setId(1);
        cDTO.setName("Name");
        cDTO.setReqName("ReqName");
        cDTO.setDeleted(null);
        service.saveCategory(cDTO);
    }
    @Test
    public void findAll() throws ItemsAssociatedException, NullParamException, NullDTOException {
        List<CategoryDTO> allCategories = service.findAll();
        if (allCategories.isEmpty()) {
            saveValid();
            findAll();
        }
        assertTrue(allCategories.size() > 0);
    }

    @Test
    public void findByReqName() throws ItemsAssociatedException, NullParamException, NullDTOException {
        CategoryDTO found = service.findByName("TestCategoryName");
        if (found == null) {
            saveValid();
            findByReqName();
        } else {
            assertEquals(found.getDeleted(), false);
        }
    }

    @Test
    public void findByNameContaining() throws ItemsAssociatedException, NullParamException, NullDTOException {
        List<CategoryDTO> found = service.findByNameContaining("Name");
        if (found.isEmpty()) {
            saveValid();
            findByNameContaining();
        }
        assertTrue(found.size() > 0);
    }

    @Test
    public void findByName() throws ItemsAssociatedException, NullParamException, NullDTOException {
        CategoryDTO found = service.findByName("TestCategoryName");
        if (found == null) {
            saveValid();
            findByName();
        } else {
            assertEquals(found.getDeleted(), false);
        }
    }

    @Test
    public void markAsDeleted() throws ItemsAssociatedException, NullDTOException, NullParamException {
        CategoryDTO found = service.findByName("TestCategoryName");
        if (found == null) {
            saveValid();
            markAsDeleted();
        } else {
            service.markCategoryAsDeleted(found.getId());
            CategoryDTO changed = service.findByName("TestCategoryName");
            assertEquals(changed.getDeleted(), true);
        }
    }

    @Test
    public void delete() throws ItemsAssociatedException, NullDTOException, NullParamException {
        CategoryDTO found = service.findByName("TestCategoryName");
        if (found == null) {
            saveValid();
            delete();
        } else {
        service.deleteCategory(found);
        }
    }
}