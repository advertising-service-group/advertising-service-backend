package ru.adservice.backend.services;

import static org.junit.Assert.*;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import ru.adservice.backend.Main;
import ru.adservice.backend.dto.*;
import ru.adservice.backend.exceptions.*;
import java.math.BigDecimal;
import java.util.List;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest(classes = Main.class)
@WebAppConfiguration
public class DefaultBannersServiceTest {
    @Autowired
    private DefaultBannersService service;
    @Autowired
    private DefaultCategoriesService categoriesService;
    @Autowired
    private DefaultUsersService usersService;

    @Test(expected = NullDTOException.class)
    public void saveNullDTO() throws NullDTOException, ValidationException, InvalidIDException, NullParamException {
        service.saveBanner(null);
    }

    @Test(expected = NullParamException.class)
    public void saveNullName() throws NullDTOException, ValidationException, InvalidIDException, NullParamException {
        BannerDTO dto = new BannerDTO();
        dto.setId(1);
        dto.setName(null);
        service.saveBanner(dto);
    }

    @Test(expected = NullParamException.class)
    public void saveEmptyName() throws NullDTOException, ValidationException, InvalidIDException, NullParamException {
        BannerDTO dto = new BannerDTO();
        dto.setId(1);
        dto.setName("");
        service.saveBanner(dto);
    }

    @Test(expected = NullParamException.class)
    public void saveNullPrice() throws NullDTOException, ValidationException, InvalidIDException, NullParamException {
        BannerDTO dto = new BannerDTO();
        dto.setId(1);
        dto.setName("TestBannerName");
        dto.setPrice(null);
        service.saveBanner(dto);
    }

    @Test(expected = NullParamException.class)
    public void saveNullContent() throws NullDTOException, ValidationException, InvalidIDException, NullParamException {
        BannerDTO dto = new BannerDTO();
        dto.setId(1);
        dto.setName("TestBannerName");
        dto.setPrice(BigDecimal.valueOf(100));
        dto.setContent(null);
        service.saveBanner(dto);
    }

    @Test(expected = NullParamException.class)
    public void saveNullCategoryId() throws NullDTOException, ValidationException, InvalidIDException, NullParamException {
        BannerDTO dto = new BannerDTO();
        dto.setId(1);
        dto.setName("TestBannerName");
        dto.setPrice(BigDecimal.valueOf(100));
        dto.setContent("TestBannerContent");
        dto.setCategoryId(null);
        service.saveBanner(dto);
    }

    @Test(expected = NullParamException.class)
    public void saveNullAuthorId() throws NullDTOException, ValidationException, InvalidIDException, NullParamException {
        BannerDTO dto = new BannerDTO();
        dto.setId(1);
        dto.setName("TestBannerName");
        dto.setPrice(BigDecimal.valueOf(100));
        dto.setContent("TestBannerContent");
        dto.setCategoryId(1);
        dto.setAuthorId(null);
        service.saveBanner(dto);
    }

    @Test(expected = NullParamException.class)
    public void saveNullDeleted() throws NullDTOException, ValidationException, InvalidIDException, NullParamException {
        BannerDTO dto = new BannerDTO();
        dto.setId(1);
        dto.setName("TestBannerName");
        dto.setPrice(BigDecimal.valueOf(100));
        dto.setContent("TestBannerContent");
        dto.setCategoryId(1);
        dto.setAuthorId(1);
        dto.setDeleted(null);
        service.saveBanner(dto);
    }

    @Test
    public void saveValid() throws NullDTOException, NullParamException, InvalidIDException{
        try {
            List<CategoryDTO> categories = categoriesService.findAll();
            List<UserDTO> users = usersService.findAll();
            if (categories.isEmpty() || users.isEmpty()) {
                return;
            }
            BannerDTO dto = new BannerDTO();
            dto.setName("TestBannerName");
            dto.setPrice(BigDecimal.valueOf(100));
            dto.setContent("TestBannerContent");
            dto.setCategoryId(categories.get(0).getId());
            dto.setAuthorId(users.get(0).getId());
            dto.setDeleted(false);
            BannerDTO savedDTO = service.saveBanner(dto);
            assertEquals(dto.getName(), savedDTO.getName());
            assertEquals(dto.getPrice(), savedDTO.getPrice());
            assertEquals(dto.getContent(), savedDTO.getContent());
            assertEquals(dto.getCategoryId(), savedDTO.getCategoryId());
            assertEquals(dto.getAuthorId(), savedDTO.getAuthorId());
            assertEquals(dto.getDeleted(), savedDTO.getDeleted());
        } catch (ValidationException e) {
            delete();
            saveValid();
        }
    }

    @Test
    public void markAsDeleted() throws InvalidIDException, NullDTOException, NullParamException {
        BannerDTO foundDTO = service.findByName("TestBannerName");
        if (foundDTO == null) {
            saveValid();
            markAsDeleted();
        } else {
            service.markBannerAsDeleted(foundDTO.getId());
            BannerDTO changedDTO = service.findByName("TestBannerName");
            assertEquals(changedDTO.getDeleted(), true);
        }
    }

    @Test
    public void delete() throws InvalidIDException, NullDTOException, NullParamException {
        BannerDTO foundDTO = service.findByName("TestBannerName");
        if (foundDTO == null) {
            saveValid();
            delete();
        } else {
            service.deleteBanner(foundDTO);
        }
    }

    @Test
    public void findById() throws InvalidIDException, NullDTOException, NullParamException {
        BannerDTO foundByNameDTO= service.findByName("TestBannerName");
        if (foundByNameDTO == null) {
            saveValid();
            findById();
        }
        else {
            BannerDTO foundByIdDTO = service.findById(foundByNameDTO.getId());
            assertEquals(foundByIdDTO.getName(), "TestBannerName");
        }
    }

    @Test
    public void findByName() throws InvalidIDException, NullDTOException, NullParamException {
        BannerDTO foundDTO = service.findByName("TestBannerName");
        if (foundDTO == null) {
            saveValid();
            findByName();
        }
        else {
            assertEquals(foundDTO.getName(), "TestBannerName");
        }
    }

    @Test
    public void findByNameContaining() throws InvalidIDException, NullDTOException, NullParamException {
        List<BannerDTO> foundBanners = service.findByNameContaining("TestBannerName");
        if (foundBanners.isEmpty()) {
            saveValid();
            findByNameContaining();
        }
        assertTrue(foundBanners.size() > 0);
    }

    @Test
    public void findByCategory() throws InvalidIDException, NullDTOException, NullParamException {
        List<CategoryDTO> categories = categoriesService.findAll();
        List<BannerDTO> foundBanners = service.findByCategory(categories.get(0).getId());
        if (foundBanners.isEmpty()) {
            saveValid();
            findByCategory();
        }
        assertTrue(foundBanners.size() > 0);
    }

    @Test
    public void findByAuthor() throws InvalidIDException, NullDTOException, NullParamException {
        List<UserDTO> users = usersService.findAll();
        List<BannerDTO> foundBanners = service.findByAuthor(users.get(0).getId());
        if (foundBanners.isEmpty()) {
            saveValid();
            findByAuthor();
        }
        assertTrue(foundBanners.size() > 0);
    }

    @Test
    public void findAll() throws InvalidIDException, NullDTOException, NullParamException {
        List<BannerDTO> foundBanners = service.findAll();
        if (foundBanners.isEmpty()) {
            saveValid();
            findAll();
        }
        assertTrue(foundBanners.size() > 0);
    }
}