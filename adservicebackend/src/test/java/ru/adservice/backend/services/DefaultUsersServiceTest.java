package ru.adservice.backend.services;

import static org.junit.Assert.*;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import ru.adservice.backend.Main;
import ru.adservice.backend.dto.UserDTO;
import ru.adservice.backend.exceptions.*;
import java.util.List;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest(classes = Main.class)
@WebAppConfiguration
public class DefaultUsersServiceTest {
    @Autowired
    private DefaultUsersService service;

    @Test(expected = NullDTOException.class)
    public void saveNullDTO() throws NullDTOException, NullParamException, InvalidIDException, ValidationException {
        service.saveUser(null);
    }

    @Test(expected = NullParamException.class)
    public void saveNullLogin() throws NullDTOException, NullParamException, InvalidIDException, ValidationException {
        UserDTO dto = new UserDTO();
        dto.setId(1);
        dto.setLogin(null);
        service.saveUser(dto);
    }

    @Test(expected = NullParamException.class)
    public void saveEmptyLogin() throws NullDTOException, NullParamException, InvalidIDException, ValidationException {
        UserDTO dto = new UserDTO();
        dto.setId(1);
        dto.setLogin("");
        service.saveUser(dto);
    }

    @Test(expected = NullParamException.class)
    public void saveNullPassword() throws NullDTOException, NullParamException, InvalidIDException, ValidationException {
        UserDTO dto = new UserDTO();
        dto.setId(1);
        dto.setLogin("TestUserLogin");
        dto.setPassword(null);
        service.saveUser(dto);
    }

    @Test(expected = NullParamException.class)
    public void saveEmptyPassword() throws NullDTOException, NullParamException, InvalidIDException, ValidationException {
        UserDTO dto = new UserDTO();
        dto.setId(1);
        dto.setLogin("TestUserLogin");
        dto.setPassword("");
        service.saveUser(dto);
    }

    @Test(expected = NullParamException.class)
    public void saveNullRoleId() throws NullDTOException, NullParamException, InvalidIDException, ValidationException {
        UserDTO dto = new UserDTO();
        dto.setId(1);
        dto.setLogin("TestUserLogin");
        dto.setPassword("TestUserPassword");
        dto.setRoleId(null);
        service.saveUser(dto);
    }

    @Test
    public void saveValid() throws NullDTOException, NullParamException, InvalidIDException {
        try {
            UserDTO dto = new UserDTO();
            dto.setLogin("TestUserLogin");
            dto.setPassword("testUserPassword");
            dto.setRoleId(1);
            UserDTO savedDTO = service.saveUser(dto);
            assertEquals(dto.getLogin(), savedDTO.getLogin());
            assertEquals(dto.getRoleId(), savedDTO.getRoleId());
        } catch (ValidationException e) {
            delete();
            saveValid();
        }
    }

    @Test
    public void delete() throws InvalidIDException, NullDTOException, NullParamException {
        UserDTO foundDTO = service.findByLogin("TestUserLogin");
        if (foundDTO == null) {
            saveValid();
            delete();
        }
        else {
            service.deleteUser(foundDTO.getId());
        }
    }

    @Test
    public void findById() throws InvalidIDException, NullDTOException, NullParamException {
        UserDTO foundByLoginDTO = service.findByLogin("TestUserLogin");
        if (foundByLoginDTO == null) {
            saveValid();
            findById();
        }
        else {
            UserDTO foundById = service.findByID(foundByLoginDTO.getId());
            assertEquals(foundById.getLogin(), "TestUserLogin");
        }
    }

    @Test
    public void findByLogin() throws InvalidIDException, NullDTOException, NullParamException {
        UserDTO foundDTO = service.findByLogin("TestUserLogin");
        if (foundDTO == null) {
            saveValid();
            findByLogin();
        }
        else {
            assertEquals(foundDTO.getLogin(), "TestUserLogin");
        }
    }

    @Test
    public void findAll() throws InvalidIDException, NullDTOException, NullParamException {
        List<UserDTO> foundUsers = service.findAll();
        if (foundUsers.isEmpty()) {
            saveValid();
            findAll();
        }
        assertTrue(foundUsers.size() > 0);
    }
}