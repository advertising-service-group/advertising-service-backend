package ru.adservice.backend.services;

import static org.junit.Assert.*;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import ru.adservice.backend.Main;
import ru.adservice.backend.dto.RoleDTO;
import ru.adservice.backend.exceptions.*;
import java.util.List;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest(classes = Main.class)
@WebAppConfiguration
public class DefaultRolesServiceTest {
    @Autowired
    private DefaultRolesService service;

    @Test(expected = NullDTOException.class)
    public void saveNullDTO() throws NullParamException, NullDTOException, ValidationException {
        service.saveRole(null);
    }

    @Test(expected = NullParamException.class)
    public void saveNullName() throws NullParamException, NullDTOException, ValidationException {
        RoleDTO dto = new RoleDTO();
        dto.setId(1);
        dto.setName(null);
        service.saveRole(dto);
    }

    @Test(expected = NullParamException.class)
    public void saveEmptyName() throws NullParamException, NullDTOException, ValidationException {
        RoleDTO dto = new RoleDTO();
        dto.setId(1);
        dto.setName("");
        service.saveRole(dto);
    }

    @Test
    public void saveValid() throws NullParamException, NullDTOException {
        try {
            RoleDTO testDTO = new RoleDTO();
            testDTO.setName("TestRoleName");
            RoleDTO saved = service.saveRole(testDTO);
            assertEquals(testDTO.getName(), saved.getName());
        } catch (ValidationException ve) {
            // test role already exists -> re-saving
            delete();
            saveValid();
        }
    }

    @Test
    public void delete() throws NullDTOException, NullParamException {
        RoleDTO found = service.findByName("TestRoleName");
        if (found == null) {
            saveValid();
            delete();
        } else {
            service.deleteRole(found.getId());
        }
    }

    @Test
    public void findAll() throws NullDTOException, NullParamException {
        List<RoleDTO> allRoles = service.findAll();
        if (allRoles.isEmpty()) {
            saveValid();
            findAll();
        }
        assertTrue(allRoles.size() > 0);
    }

    @Test
    public void findByName() throws NullDTOException, NullParamException {
        RoleDTO found = service.findByName("TestRoleName");
        if (found == null) {
            saveValid();
            findByName();
        } else {
            assertEquals(found.getName(), "TestRoleName");
        }
    }

    @Test
    public void findByID() throws NullDTOException, NullParamException {
        RoleDTO foundByName = service.findByName("TestRoleName");
        if (foundByName == null) {
            saveValid();
            findByID();
        } else {
            RoleDTO foundByID = service.findByID(foundByName.getId());
            assertEquals(foundByID.getName(), "TestRoleName");
        }
    }
}