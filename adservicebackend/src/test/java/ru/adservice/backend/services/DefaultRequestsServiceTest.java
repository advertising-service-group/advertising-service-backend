package ru.adservice.backend.services;

import static org.junit.Assert.*;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import ru.adservice.backend.Main;
import ru.adservice.backend.dto.*;
import ru.adservice.backend.dto.RequestDTO;
import ru.adservice.backend.exceptions.*;
import javax.servlet.*;
import javax.servlet.http.*;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.math.BigDecimal;
import java.security.Principal;
import java.sql.Date;
import java.util.*;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest(classes = Main.class)
@WebAppConfiguration
public class DefaultRequestsServiceTest {
    @Autowired
    private DefaultRequestsService requestsService;

    @Autowired
    private DefaultBannersService bannersService;

    @Autowired
    private DefaultCategoriesService categoriesService;

    private int savedBannerID;
    private RequestDTO requestDTO;
    private HttpServletRequest request;

    @Test
    public void save() throws InvalidIDException, ValidationException, NullDTOException, NullParamException, ItemsAssociatedException {
        saveTestCategoryAndBanner();
        createRequestDTO();

        requestsService.saveRequest(requestDTO);
        List<RequestDTO> found = requestsService.findByIPAddress("TestIP");
        assertEquals(1, found.size());
        assertEquals(found.get(0).getBannerId(), requestDTO.getBannerId());
        assertEquals(found.get(0).getUserAgent(), requestDTO.getUserAgent());

        deleteTestCategoryBannerAndRequestIfExist();
    }

    @Test
    public void findBestBanner() throws NullDTOException, ValidationException, InvalidIDException, NullParamException, ItemsAssociatedException {
        saveTestCategoryAndBanner();
        createRequest();

        BannerDTO best = requestsService.findBestBannerForCategory("TestCategoryReqName", request);
        assertEquals(best.getContent(), "TestBannerContent");

        deleteTestCategoryBannerAndRequestIfExist();
    }

    @Test
    public void findAll() throws NullDTOException, NullParamException, InvalidIDException, ValidationException, ItemsAssociatedException {
        findBestBanner();

        List<RequestDTO> requests = requestsService.findAll();
        assertTrue(requests.size() > 0);

        deleteTestCategoryBannerAndRequestIfExist();
    }

    private void createRequest() {
        request = new HttpServletRequest() {
            @Override
            public String getAuthType() { return null; }
            @Override
            public Cookie[] getCookies() { return new Cookie[0]; }
            @Override
            public long getDateHeader(String s) { return 0; }

            @Override
            public String getHeader(String s) {
                return "TestUserAgent";
            }

            @Override
            public Enumeration<String> getHeaders(String s) { return null; }
            @Override
            public Enumeration<String> getHeaderNames() { return null; }
            @Override
            public int getIntHeader(String s) { return 0; }
            @Override
            public String getMethod() { return null; }
            @Override
            public String getPathInfo() { return null; }
            @Override
            public String getPathTranslated() { return null; }
            @Override
            public String getContextPath() { return null; }
            @Override
            public String getQueryString() { return null; }
            @Override
            public String getRemoteUser() { return null; }
            @Override
            public boolean isUserInRole(String s) { return false; }
            @Override
            public Principal getUserPrincipal() { return null; }
            @Override
            public String getRequestedSessionId() { return null; }
            @Override
            public String getRequestURI() { return null; }
            @Override
            public StringBuffer getRequestURL() { return null; }
            @Override
            public String getServletPath() { return null; }
            @Override
            public HttpSession getSession(boolean b) { return null; }
            @Override
            public HttpSession getSession() { return null; }
            @Override
            public String changeSessionId() { return null; }
            @Override
            public boolean isRequestedSessionIdValid() { return false; }
            @Override
            public boolean isRequestedSessionIdFromCookie() { return false; }
            @Override
            public boolean isRequestedSessionIdFromURL() { return false; }
            @Override
            public boolean isRequestedSessionIdFromUrl() { return false; }
            @Override
            public boolean authenticate(HttpServletResponse httpServletResponse) throws IOException, ServletException { return false; }
            @Override
            public void login(String s, String s1) throws ServletException {}
            @Override
            public void logout() throws ServletException {}
            @Override
            public Collection<Part> getParts() throws IOException, ServletException { return null; }
            @Override
            public Part getPart(String s) throws IOException, ServletException { return null; }
            @Override
            public <T extends HttpUpgradeHandler> T upgrade(Class<T> aClass) throws IOException, ServletException { return null; }
            @Override
            public Object getAttribute(String s) { return null; }
            @Override
            public Enumeration<String> getAttributeNames() { return null; }
            @Override
            public String getCharacterEncoding() { return null; }
            @Override
            public void setCharacterEncoding(String s) throws UnsupportedEncodingException {}
            @Override
            public int getContentLength() { return 0; }
            @Override
            public long getContentLengthLong() { return 0; }
            @Override
            public String getContentType() { return null; }
            @Override
            public ServletInputStream getInputStream() throws IOException { return null; }
            @Override
            public String getParameter(String s) { return null; }
            @Override
            public Enumeration<String> getParameterNames() { return null; }
            @Override
            public String[] getParameterValues(String s) { return new String[0]; }
            @Override
            public Map<String, String[]> getParameterMap() { return null; }
            @Override
            public String getProtocol() { return null; }
            @Override
            public String getScheme() { return null; }
            @Override
            public String getServerName() { return null; }
            @Override
            public int getServerPort() { return 0; }
            @Override
            public BufferedReader getReader() throws IOException { return null; }

            @Override
            public String getRemoteAddr() {
                return "TestIP";
            }

            @Override
            public String getRemoteHost() { return null; }
            @Override
            public void setAttribute(String s, Object o) {}
            @Override
            public void removeAttribute(String s) {}
            @Override
            public Locale getLocale() { return null; }
            @Override
            public Enumeration<Locale> getLocales() { return null; }
            @Override
            public boolean isSecure() { return false; }
            @Override
            public RequestDispatcher getRequestDispatcher(String s) { return null; }
            @Override
            public String getRealPath(String s) { return null; }
            @Override
            public int getRemotePort() { return 0; }
            @Override
            public String getLocalName() { return null; }
            @Override
            public String getLocalAddr() { return null; }
            @Override
            public int getLocalPort() { return 0; }
            @Override
            public ServletContext getServletContext() { return null; }
            @Override
            public AsyncContext startAsync() throws IllegalStateException { return null; }
            @Override
            public AsyncContext startAsync(ServletRequest servletRequest, ServletResponse servletResponse) throws IllegalStateException { return null; }
            @Override
            public boolean isAsyncStarted() { return false; }
            @Override
            public boolean isAsyncSupported() { return false; }
            @Override
            public AsyncContext getAsyncContext() { return null; }
            @Override
            public DispatcherType getDispatcherType() { return null; }
        };
    }

    private void createRequestDTO() {
        String userAgent = "TestUserAgent";
        String ip = "TestIP";
        Date currentDate = new Date(Calendar.getInstance().getTime().getTime());

        requestDTO = new RequestDTO();
        requestDTO.setBannerId(savedBannerID);
        requestDTO.setUserAgent(userAgent);
        requestDTO.setIpAddress(ip);
        requestDTO.setDate(currentDate);
    }

    private void saveTestCategoryAndBanner() throws NullParamException, NullDTOException, ValidationException, InvalidIDException, ItemsAssociatedException {
        deleteTestCategoryBannerAndRequestIfExist();

        CategoryDTO categoryDTO = new CategoryDTO();
        categoryDTO.setName("TestCategoryName");
        categoryDTO.setReqName("TestCategoryReqName");
        categoryDTO.setDeleted(false);
        CategoryDTO savedCategory = categoriesService.saveCategory(categoryDTO);

        BannerDTO bannerDTO = new BannerDTO();
        bannerDTO.setName("TestBannerName");
        bannerDTO.setPrice(BigDecimal.valueOf(100));
        bannerDTO.setContent("TestBannerContent");
        bannerDTO.setCategoryId(savedCategory.getId());
        bannerDTO.setAuthorId(1);
        bannerDTO.setDeleted(false);
        BannerDTO savedBanner = bannersService.saveBanner(bannerDTO);

        savedBannerID = savedBanner.getId();
    }

    public void deleteTestCategoryBannerAndRequestIfExist() throws ItemsAssociatedException, InvalidIDException {
        List<RequestDTO> existingRequests = requestsService.findByIPAddress("TestIP");
        if (!existingRequests.isEmpty()) {
            int originalSize = existingRequests.size();
            for (int i = 1; i <= existingRequests.size(); i++) {
                requestsService.delete(existingRequests.get(originalSize - i));
            }
        }

        BannerDTO existingBanner = bannersService.findByName("TestBannerName");
        if (existingBanner != null) {
            bannersService.deleteBanner(existingBanner);
        }

        CategoryDTO existingCategory = categoriesService.findByName("TestCategoryName");
        if (existingCategory != null) {
            categoriesService.deleteCategory(existingCategory);
        }
    }
}