package ru.adservice.backend.services;

import ru.adservice.backend.dto.BannerDTO;
import ru.adservice.backend.exceptions.*;
import java.util.List;

public interface BannersService {
    BannerDTO saveBanner(BannerDTO dto) throws NullDTOException, NullParamException, ValidationException, InvalidIDException;
    void markBannerAsDeleted(Integer id);
    void deleteBanner(BannerDTO dto) throws InvalidIDException;
    BannerDTO findById(Integer id);
    BannerDTO findByName(String name);
    List<BannerDTO> findByNameContaining(String name);
    List<BannerDTO> findByCategory(Integer id);
    List<BannerDTO> findByAuthor(Integer id);
    List<BannerDTO> findAll();
}