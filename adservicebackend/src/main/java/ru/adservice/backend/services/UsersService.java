package ru.adservice.backend.services;

import ru.adservice.backend.dto.*;
import ru.adservice.backend.exceptions.*;
import java.util.List;

public interface UsersService {
    UserDTO saveUser(UserDTO dto) throws ValidationException, NullDTOException, NullParamException, InvalidIDException;
    void deleteUser(Integer id);
    UserDTO findByID(Integer id);
    UserDTO findByLogin(String login);
    List<UserDTO> findAll();
    UserMainInfoDTO getCurrentUser();
}