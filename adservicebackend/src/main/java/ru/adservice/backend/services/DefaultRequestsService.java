package ru.adservice.backend.services;

import lombok.extern.java.Log;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.adservice.backend.converters.RequestsConverter;
import ru.adservice.backend.dto.*;
import ru.adservice.backend.entity.Request;
import ru.adservice.backend.exceptions.InvalidIDException;
import ru.adservice.backend.repositories.RequestsRepository;
import javax.servlet.http.HttpServletRequest;
import java.sql.Date;
import java.util.*;
import java.util.stream.Collectors;

@Service
@Log
public class DefaultRequestsService implements RequestsService {
    private final CategoriesService categoriesService;
    private final BannersService bannersService;
    private final RequestsRepository repository;
    private final RequestsConverter converter;

    @Autowired
    public DefaultRequestsService(
            CategoriesService categoriesService,
            BannersService bannersService,
            RequestsRepository repository,
            RequestsConverter converter) {
        this.repository = repository;
        this.converter = converter;
        this.bannersService = bannersService;
        this.categoriesService = categoriesService;
    }

    @Override
    public RequestDTO saveRequest(RequestDTO dto) throws InvalidIDException {
        Request savedRequest = repository.save(converter.dtoToRequest(dto));
        return converter.requestToDTO(savedRequest);
    }

    @Override
    public BannerDTO findBestBannerForCategory(String categoryName, HttpServletRequest request) throws InvalidIDException {
        CategoryDTO categoryDTO = categoriesService.findByReqName(categoryName);
        if (categoryDTO == null) {
            return null;
        }

        List<BannerDTO> bannerDTOs = bannersService.findByCategory(categoryDTO.getId());
        if (bannerDTOs == null) {
            return null;
        }

        String userAgent = request.getHeader("User-Agent");
        String ip = request.getRemoteAddr();
        Date currentDate = new Date(Calendar.getInstance().getTime().getTime());

        List<RequestDTO> requestDTOs = findByIPAddress(ip);
        requestDTOs.removeIf(dto -> !(dto.getUserAgent().equals(userAgent) || dto.getDate().equals(currentDate)));

        Set<Integer> shownBannersIDs = new HashSet<>();
        for (RequestDTO dto : requestDTOs) {
            shownBannersIDs.add(dto.getBannerId());
        }

        bannerDTOs.removeIf(dto -> (shownBannersIDs.contains(dto.getId()) || dto.getDeleted()));
        if (bannerDTOs.isEmpty()) {
            for (RequestDTO dto : requestDTOs) {
                repository.delete(converter.dtoToRequest(dto));
            }
            bannerDTOs = bannersService.findByCategory(categoryDTO.getId());
        }

        BannerDTO best = bannerDTOs.get(0);
        for (BannerDTO bannerDTO : bannerDTOs) {
            if (bannerDTO.getPrice().compareTo(best.getPrice()) > 0) {
                best = bannerDTO;
            }
        }

        RequestDTO requestDTO = new RequestDTO();
        requestDTO.setBannerId(best.getId());
        requestDTO.setUserAgent(userAgent);
        requestDTO.setIpAddress(ip);
        requestDTO.setDate(new Date(Calendar.getInstance().getTime().getTime()));
        saveRequest(requestDTO);

        return best;
    }

    @Override
    public List<RequestDTO> findByIPAddress(String ip) {
        List<Request> requests = repository.findByIpAddress(ip);
        List<RequestDTO> DTOs = new LinkedList<>();
        if (requests != null) {
            for (Request request : requests) {
                DTOs.add(converter.requestToDTO(request));
            }
            return DTOs;
        }
        return null;
    }

    @Override
    public List<RequestDTO> findAll() {
        return repository
                .findAll()
                .stream()
                .map(converter::requestToDTO)
                .collect(Collectors.toList());
    }

    @Override
    public void delete(RequestDTO dto) throws InvalidIDException {
        repository.delete(converter.dtoToRequest(dto));
    }
}