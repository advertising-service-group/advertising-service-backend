package ru.adservice.backend.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.adservice.backend.converters.BannersConverter;
import ru.adservice.backend.dto.BannerDTO;
import ru.adservice.backend.entity.Banner;
import ru.adservice.backend.exceptions.*;
import ru.adservice.backend.repositories.BannersRepository;
import java.util.*;
import java.util.stream.Collectors;

@Service
public class DefaultBannersService implements BannersService {
    private final BannersRepository bannersRepository;
    private final BannersConverter bannersConverter;

    @Autowired
    public DefaultBannersService(BannersRepository bannersRepository,
                                 BannersConverter bannersConverter) {
        this.bannersRepository = bannersRepository;
        this.bannersConverter = bannersConverter;
    }

    @Override
    public BannerDTO saveBanner(BannerDTO dto) throws NullDTOException, NullParamException, ValidationException, InvalidIDException {
        validateBannerDTO(dto);
        Banner savedBanner = bannersRepository.save(bannersConverter.dtoToBanner(dto));
        return bannersConverter.bannerToDTO(savedBanner);
    }

    @Override
    public void markBannerAsDeleted(Integer id) {
        Optional<Banner> banner = bannersRepository.findById(id);
        if (banner.isPresent()) {
            Banner oldBanner = banner.get();
            Banner newBanner = new Banner();
            newBanner.setId(oldBanner.getId());
            newBanner.setName(oldBanner.getName());
            newBanner.setPrice(oldBanner.getPrice());
            newBanner.setContent(oldBanner.getContent());
            newBanner.setCategory(oldBanner.getCategory());
            newBanner.setAuthor(oldBanner.getAuthor());
            newBanner.setDeleted(true);
            bannersRepository.save(newBanner);
        }
    }

    @Override
    public void deleteBanner(BannerDTO dto) throws InvalidIDException {
        bannersRepository.delete(bannersConverter.dtoToBanner(dto));
    }

    @Override
    public BannerDTO findById(Integer id) {
        Optional<Banner> banner = bannersRepository.findById(id);
        return banner.map(bannersConverter::bannerToDTO).orElse(null);
    }

    @Override
    public BannerDTO findByName(String name) {
        Banner banner = bannersRepository.findByName(name);
        return bannersConverter.bannerToDTO(banner);
    }

    @Override
    public List<BannerDTO> findByNameContaining(String name) {
        return bannersRepository.findByNameContaining(name)
                .stream()
                .map(bannersConverter::bannerToDTO)
                .collect(Collectors.toList());
    }

    @Override
    public List<BannerDTO> findByCategory(Integer id) {
        List<Banner> banners = bannersRepository.findByCategoryId(id);
        List<BannerDTO> DTOs = new LinkedList<>();
        if (banners != null) {
            for (Banner banner : banners) {
                DTOs.add(bannersConverter.bannerToDTO(banner));
            }
            return DTOs;
        }
        return null;
    }

    @Override
    public List<BannerDTO> findByAuthor(Integer id) {
        List<Banner> banners = bannersRepository.findByAuthorId(id);
        List<BannerDTO> DTOs = new LinkedList<>();
        if (banners != null) {
            for (Banner banner : banners) {
                DTOs.add(bannersConverter.bannerToDTO(banner));
            }
            return DTOs;
        }
        return null;
    }

    @Override
    public List<BannerDTO> findAll() {
        return bannersRepository
                .findAll()
                .stream()
                .map(bannersConverter::bannerToDTO)
                .collect(Collectors.toList());
    }

    public void validateBannerDTO(BannerDTO bannerDTO) throws NullDTOException, NullParamException, ValidationException {
        if (bannerDTO == null) {
            throw new NullDTOException("Banner DTO is null");
        }
        if (bannerDTO.getName() == null || bannerDTO.getName().isEmpty()) {
            throw new NullParamException("Name in banner is empty");
        }
        if (bannerDTO.getPrice() == null) {
            throw new NullParamException("Banner has no is empty");
        }
        if (bannerDTO.getContent() == null) {
            throw new NullParamException("Banner has no content");
        }
        if (bannerDTO.getCategoryId() == null) {
            throw new NullParamException("Banner has no category");
        }
        if (bannerDTO.getAuthorId() == null) {
            throw new NullParamException("Banner has no author");
        }
        if (bannerDTO.getDeleted() == null) {
            throw new NullParamException("The banner has an empty delete field");
        }
        BannerDTO withName = findByName(bannerDTO.getName());
        if (withName != null && !withName.getId().equals(bannerDTO.getId())) {
            throw new ValidationException("Banner with name '" + bannerDTO.getName() +"' already exist in database");
        }
    }
}