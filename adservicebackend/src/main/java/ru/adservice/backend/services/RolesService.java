package ru.adservice.backend.services;

import ru.adservice.backend.dto.RoleDTO;
import ru.adservice.backend.exceptions.*;
import java.util.List;

public interface RolesService {
    RoleDTO saveRole(RoleDTO dto) throws ValidationException, NullDTOException, NullParamException;
    void deleteRole(Integer id);
    RoleDTO findByID(Integer id);
    RoleDTO findByName(String name);
    List<RoleDTO> findAll();
}