package ru.adservice.backend.services;

import lombok.RequiredArgsConstructor;
import org.springframework.security.core.context.SecurityContextHolderStrategy;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;
import ru.adservice.backend.converters.UsersConverter;
import ru.adservice.backend.dto.*;
import ru.adservice.backend.entity.*;
import ru.adservice.backend.exceptions.*;
import ru.adservice.backend.repositories.UsersRepository;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@RequiredArgsConstructor
@Service
public class DefaultUsersService implements UsersService {
    private final UsersRepository repository;
    private final UsersConverter converter;
    private final BCryptPasswordEncoder encoder;
    private final SecurityContextHolderStrategy securityContextHolderStrategy;

    @Override
    public UserDTO saveUser(UserDTO dto) throws ValidationException, NullDTOException, NullParamException, InvalidIDException {
        validateUserDTO(dto);
        UserDTO withEncodedPassword = new UserDTO();
        withEncodedPassword.setId(dto.getId());
        withEncodedPassword.setLogin(dto.getLogin());
        withEncodedPassword.setRoleId(dto.getRoleId());
        withEncodedPassword.setPassword(encoder.encode(dto.getPassword()));
        User savedUser = repository.save(converter.dtoToUser(withEncodedPassword));
        return converter.userToDTO(savedUser);
    }

    @Override
    public void deleteUser(Integer id) {
        Optional<User> user = repository.findById(id);
        user.ifPresent(repository::delete);
    }

    @Override
    public UserDTO findByID(Integer id) {
        Optional<User> user = repository.findById(id);
        return user.map(converter::userToDTO).orElse(null);
    }

    @Override
    public UserDTO findByLogin(String login) {
        User user = repository.findByLogin(login);
        return converter.userToDTO(user);
    }

    @Override
    public List<UserDTO> findAll() {
        return repository
                .findAll()
                .stream()
                .map(converter::userToDTO)
                .collect(Collectors.toList());
    }

    private void validateUserDTO(UserDTO userDTO) throws NullDTOException, NullParamException, ValidationException {
        if (userDTO == null) {
            throw new NullDTOException("User DTO is null");
        }
        if (userDTO.getLogin() == null || userDTO.getLogin().isEmpty()) {
            throw new NullParamException("Login in user is empty");
        }
        if (userDTO.getPassword() == null || userDTO.getPassword().isEmpty()) {
            throw new NullParamException("Password in user is empty");
        }
        if (userDTO.getRoleId() == null) {
            throw new NullParamException("Role ID in user is null");
        }
        UserDTO withLogin = findByLogin(userDTO.getLogin());
        if (withLogin != null && !withLogin.getId().equals(userDTO.getId())) {
            throw new ValidationException("User with login '" + userDTO.getLogin() + "' already exists in database");
        }
    }

    public UserMainInfoDTO getCurrentUser() {
        String login = securityContextHolderStrategy.getContext().getAuthentication().getName();
        User user = repository.findByLogin(login);
        Role role = user.getRole();
        UserMainInfoDTO mainInfoDTO = new UserMainInfoDTO();
        mainInfoDTO.setId(user.getId());
        mainInfoDTO.setLogin(user.getLogin());
        mainInfoDTO.setRoleID(role.getId());
        mainInfoDTO.setRoleName(role.getName());
        return mainInfoDTO;
    }
}