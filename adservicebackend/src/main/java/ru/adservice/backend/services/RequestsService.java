package ru.adservice.backend.services;

import ru.adservice.backend.dto.*;
import ru.adservice.backend.exceptions.InvalidIDException;
import javax.servlet.http.HttpServletRequest;
import java.util.List;

public interface RequestsService {
    RequestDTO saveRequest(RequestDTO dto) throws InvalidIDException;
    BannerDTO findBestBannerForCategory(String categoryName, HttpServletRequest request) throws InvalidIDException;
    List<RequestDTO> findByIPAddress(String ip);
    List<RequestDTO> findAll();
    void delete(RequestDTO dto) throws InvalidIDException;
}