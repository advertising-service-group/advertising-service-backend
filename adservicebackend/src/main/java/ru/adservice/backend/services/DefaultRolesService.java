package ru.adservice.backend.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.adservice.backend.converters.RolesConverter;
import ru.adservice.backend.dto.RoleDTO;
import ru.adservice.backend.entity.Role;
import ru.adservice.backend.exceptions.*;
import ru.adservice.backend.repositories.RolesRepository;
import java.util.*;
import java.util.stream.Collectors;

@Service
public class DefaultRolesService implements RolesService {
    private final RolesRepository repository;
    private final RolesConverter converter;

    @Autowired
    public DefaultRolesService(
            RolesRepository repository,
            RolesConverter converter) {
        this.converter = converter;
        this.repository = repository;
    }

    @Override
    public RoleDTO saveRole(RoleDTO dto) throws ValidationException, NullDTOException, NullParamException {
        validateRoleDTO(dto);
        Role savedRole = repository.save(converter.dtoToRole(dto));
        return converter.roleToDTO(savedRole);
    }

    @Override
    public void deleteRole(Integer id) {
        Optional<Role> role = repository.findById(id);
        role.ifPresent(repository::delete);
    }

    @Override
    public RoleDTO findByID(Integer id) {
        Optional<Role> role = repository.findById(id);
        return role.map(converter::roleToDTO).orElse(null);
    }

    @Override
    public RoleDTO findByName(String name) {
        Role role = repository.findByName(name);
        return converter.roleToDTO(role);
    }

    @Override
    public List<RoleDTO> findAll() {
        return repository
                .findAll()
                .stream()
                .map(converter::roleToDTO)
                .collect(Collectors.toList());
    }

    private void validateRoleDTO(RoleDTO roleDTO) throws NullDTOException, NullParamException, ValidationException {
        if (roleDTO == null) {
            throw new NullDTOException("Role DTO is null");
        }
        if (roleDTO.getName() == null || roleDTO.getName().isEmpty()) {
            throw new NullParamException("Name in role is empty");
        }
        RoleDTO withName = findByName(roleDTO.getName());
        if (withName != null && !withName.getId().equals(roleDTO.getId())) {
            throw new ValidationException("Role with name '" + roleDTO.getName() + "' already exists in database");
        }
    }
}