package ru.adservice.backend.services;

import ru.adservice.backend.dto.CategoryDTO;
import ru.adservice.backend.exceptions.*;
import java.util.List;

public interface CategoriesService {
    CategoryDTO saveCategory(CategoryDTO dto) throws ValidationException, NullDTOException, NullParamException;
    void markCategoryAsDeleted(Integer id) throws ItemsAssociatedException;
    void deleteCategory(CategoryDTO dto) throws ItemsAssociatedException;
    CategoryDTO findById(Integer id);
    CategoryDTO findByName(String name);
    List<CategoryDTO> findByNameContaining(String name);
    CategoryDTO findByReqName(String reqName);
    List<CategoryDTO> findAll();
}