package ru.adservice.backend.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.adservice.backend.converters.CategoriesConverter;
import ru.adservice.backend.dto.*;
import ru.adservice.backend.entity.Category;
import ru.adservice.backend.exceptions.*;
import ru.adservice.backend.repositories.CategoriesRepository;
import java.util.*;
import java.util.stream.Collectors;

@Service
public class DefaultCategoriesService implements CategoriesService {
    private final CategoriesRepository categoriesRepository;
    private final CategoriesConverter categoriesConverter;
    private final BannersService bannersService;

    @Autowired
    public DefaultCategoriesService(
            CategoriesRepository categoriesRepository,
            CategoriesConverter categoriesConverter,
            BannersService bannersService) {
        this.bannersService = bannersService;
        this.categoriesConverter = categoriesConverter;
        this.categoriesRepository = categoriesRepository;
    }

    @Override
    public CategoryDTO saveCategory(CategoryDTO dto) throws ValidationException, NullDTOException, NullParamException {
        validateCategoryDTO(dto);
        Category savedCategory = categoriesRepository.save(categoriesConverter.dtoToCategory(dto));
        return categoriesConverter.categoryToDTO(savedCategory);
    }

    @Override
    public void markCategoryAsDeleted(Integer id) throws ItemsAssociatedException {
        List<BannerDTO> bannersAssociated = bannersService.findByCategory(id);
        if (!bannersAssociated.isEmpty()) {
            throw new ItemsAssociatedException("Could not mark deleted category with banners associated");
        }
        Optional<Category> category = categoriesRepository.findById(id);
        if (category.isPresent()) {
            Category oldCategory = category.get();
            Category newCategory = new Category();
            newCategory.setId(oldCategory.getId());
            newCategory.setName(oldCategory.getName());
            newCategory.setReqName(oldCategory.getReqName());
            newCategory.setDeleted(true);
            categoriesRepository.save(newCategory);
        }
    }

    @Override
    public void deleteCategory(CategoryDTO dto) throws ItemsAssociatedException {
        List<BannerDTO> bannersAssociated = bannersService.findByCategory(dto.getId());
        if (!bannersAssociated.isEmpty()) {
            throw new ItemsAssociatedException("Could not delete category with banners associated");
        }
        categoriesRepository.delete(categoriesConverter.dtoToCategory(dto));
    }

    @Override
    public CategoryDTO findById(Integer id) {
        Optional<Category> category = categoriesRepository.findById(id);
        return category.map(categoriesConverter::categoryToDTO).orElse(null);
    }

    @Override
    public CategoryDTO findByName(String name) {
        Category category = categoriesRepository.findByName(name);
        return categoriesConverter.categoryToDTO(category);
    }

    @Override
    public List<CategoryDTO> findByNameContaining(String name) {
        return categoriesRepository.findByNameContaining(name)
                .stream()
                .map(categoriesConverter::categoryToDTO)
                .collect(Collectors.toList());
    }

    @Override
    public CategoryDTO findByReqName(String reqName) {
        Category category = categoriesRepository.findByReqName(reqName);
        return categoriesConverter.categoryToDTO(category);
    }

    @Override
    public List<CategoryDTO> findAll() {
        return categoriesRepository.findAll()
                .stream()
                .map(categoriesConverter::categoryToDTO)
                .collect(Collectors.toList());
    }

    public void validateCategoryDTO(CategoryDTO categoryDTO) throws NullDTOException, NullParamException, ValidationException {
        if (categoryDTO == null) {
            throw new NullDTOException("Category DTO is null");
        }
        if (categoryDTO.getName() == null || categoryDTO.getName().isEmpty()) {
            throw new NullParamException("Name in category is empty");
        }
        if (categoryDTO.getReqName() == null || categoryDTO.getReqName().isEmpty()) {
            throw new NullParamException("Request name in category is empty");
        }
        if (categoryDTO.getDeleted() == null) {
            throw new NullParamException("The category has an empty delete field");
        }
        CategoryDTO withName = findByName(categoryDTO.getName());
        if (withName != null && !withName.getId().equals(categoryDTO.getId())) {
            throw new ValidationException("Category with name '" + categoryDTO.getName() + "' already exist in database");
        }
        CategoryDTO withReqName = findByReqName(categoryDTO.getReqName());
        if (withReqName != null && !withReqName.getId().equals(categoryDTO.getId())) {
            throw new ValidationException("Category with request name '" + categoryDTO.getReqName() + "' already exist in database");
        }
    }
}