package ru.adservice.backend.controllers;

import lombok.AllArgsConstructor;
import lombok.extern.java.Log;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;
import ru.adservice.backend.dto.*;
import ru.adservice.backend.exceptions.InvalidIDException;
import ru.adservice.backend.services.RequestsService;
import javax.servlet.http.HttpServletRequest;
import java.util.List;

@RestController
@RequestMapping("/")
@AllArgsConstructor
@Log
public class RequestsController {
    private final RequestsService service;

    @GetMapping("/")
    public String main() {
        log.info("Requests :: handling :: main");
        return
"<!DOCTYPE html>" +
"<html>" +
    "<head>" +
        "<title>Ad service back-end</title>" +
    "</head>" +
    "<body>" +
        "<h1>Back-end for ad-service is working!</h1>" +
        "<h1>Visit /help to see its abilities.</h1>" +
    "</body>" +
"</html";
    }

    @GetMapping("/help")
    public String help() {
        log.info("Requests :: handling :: help");
        return
"<!DOCTYPE html>" +
"<html>" +
    "<head>" +
        "<title>Ad service back-end</title>" +
    "</head>" +
    "<body>" +
        "<h1>What our back-end is able to do right now?</h1>" +
        "<h2>1. /roles</h2>" +
        "<h2>a) /findAll</h2>" +
        "<h2>b) /findByID/id</h2>" +
        "<h2>2. /users</h2>" +
        "<h2>a) /save (requires dto)</h2>" +
        "<h2>b) /findAll</h2>" +
        "<h2>c) /findByLogin?login=login</h2>" +
        "<h2>d) /delete/id</h2>" +
        "<h2>e) /getCurrentUser</h2>" +
        "<h2>3. /banners</h2>" +
        "<h2>a) /save (requires dto)</h2>" +
        "<h2>b) /findAll</h2>" +
        "<h2>c) /findByName?name=name</h2>" +
        "<h2>d) /findByCategory/id</h2>" +
        "<h2>e) /findByAuthor/id</h2>" +
        "<h2>f) /delete/id</h2>" +
        "<h2>f) /reallyDelete/id</h2>" +
        "<h2>4. /categories</h2>" +
        "<h2>a) /save (requires dto)</h2>" +
        "<h2>b) /findAll</h2>" +
        "<h2>c) /findByName?name=name</h2>" +
        "<h2>d) /delete/id</h2>" +
        "<h2>f) /reallyDelete/id</h2>" +
        "<h2>5. /</h2>" +
        "<h2>a) /getAd?category=category_request_name</h2>" +
        "<h2>b) /getAllRequests</h2>" +
        "<h2>6. /auth</h2>" +
        "<h2>a) /register (requires dto)</h2>" +
        "<h2>[returns dto with data of registered user]</h2>" +
        "<h2>b) /login (requires dto) </h2>" +
        "<h2>[returns dto with login + token]</h2>" +
        "<h2>ACCESS:</h2>" +
        "<h2>/users/findAll, /roles/findAll, /auth, /help, / - for everybody</h2>" +
        "<h2>/users/getCurrentUser, /banners/**, /categories/findAll, /categories/findByName - for any authorized</h2>" +
        "<h2>other - for admins only</h2>" +
    "</body>" +
"</html>";
    }

    @GetMapping("/getAd")
    public String findBestBannerForCategory(@RequestParam String category, HttpServletRequest request) {
        log.info("Requests :: handling :: find best banner for category :: " + category);
        BannerDTO best = null;
        try {
            best = service.findBestBannerForCategory(category, request);
        } catch (InvalidIDException e) {
            log.info("Error :: " + e.getMessage());
        }
        if (best == null) {
            throw new ResponseStatusException(
                    HttpStatus.NO_CONTENT, "No banners available", null);
        } else {
            return best.getContent();
        }
    }

    @GetMapping("/getAllRequests")
    public List<RequestDTO> findAllUsers() {
        log.info("Requests :: handling :: get all requests");
        return service.findAll();
    }
}