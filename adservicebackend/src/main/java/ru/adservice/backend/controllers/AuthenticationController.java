package ru.adservice.backend.controllers;

import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.web.server.ResponseStatusException;
import ru.adservice.backend.converters.UsersConverter;
import ru.adservice.backend.dto.*;
import ru.adservice.backend.entity.User;
import ru.adservice.backend.exceptions.*;
import ru.adservice.backend.security.jwt.JwtTokenProvider;
import ru.adservice.backend.services.UsersService;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.*;
import org.springframework.security.core.AuthenticationException;
import org.springframework.web.bind.annotation.*;
import java.util.*;

@Slf4j
@RestController
@RequestMapping(value = "/auth")
@AllArgsConstructor
public class AuthenticationController {
    private final AuthenticationManager authenticationManager;
    private final JwtTokenProvider jwtTokenProvider;
    private final UsersService service;
    private final UsersConverter converter;

    @PostMapping("/register")
    public UserDTO register(@RequestBody UserDTO dto) {
        log.info("Authentication :: handling :: register :: " + dto);
        try {
            return service.saveUser(dto);
        } catch (ValidationException e) {
            log.info("Error :: " + e.getMessage());
            throw new ResponseStatusException(HttpStatus.CONFLICT, e.getMessage(), e);
        } catch (NullParamException | NullDTOException | InvalidIDException e) {
            log.info("Error :: " + e.getMessage());
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, e.getMessage(), e);
        }
    }

    @PostMapping("/login")
    public ResponseEntity login(@RequestBody AuthenticationRequestDTO requestDto) {
        log.info("Authentication :: handling :: login :: " + requestDto);
        try {
            String login = requestDto.getLogin();
            authenticationManager.authenticate(new UsernamePasswordAuthenticationToken(login, requestDto.getPassword()));

            User user = converter.dtoToUser(service.findByLogin(login));

            if (user == null) {
                throw new UsernameNotFoundException("User with login: " + login + " not found");
            }

            String token = jwtTokenProvider.createToken(login, user.getRole());

            Map<Object, Object> response = new HashMap<>();
            response.put("login", login);
            response.put("token", token);
            return ResponseEntity.ok(response);
        } catch (AuthenticationException | InvalidIDException e) {
            throw new BadCredentialsException("Invalid login or password");
        }
    }
}