package ru.adservice.backend.controllers;

import lombok.AllArgsConstructor;
import lombok.extern.java.Log;
import org.springframework.web.bind.annotation.*;
import ru.adservice.backend.dto.RoleDTO;
import ru.adservice.backend.services.RolesService;
import java.util.List;

@RestController
@RequestMapping("/roles")
@AllArgsConstructor
@Log
public class RolesController {
    private final RolesService service;

    @GetMapping("/findAll")
    public List<RoleDTO> findAllRoles() {
        log.info("Roles :: handling :: find all");
        return service.findAll();
    }

    @GetMapping("/findByID/{id}")
    public RoleDTO findByName(@PathVariable Integer id) {
        log.info("Roles :: handling :: find by id :: " + id);
        return service.findByID(id);
    }
}