package ru.adservice.backend.controllers;

import lombok.AllArgsConstructor;
import lombok.extern.java.Log;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;
import ru.adservice.backend.dto.UserDTO;
import ru.adservice.backend.dto.UserMainInfoDTO;
import ru.adservice.backend.exceptions.*;
import ru.adservice.backend.services.UsersService;
import java.util.List;

@RestController
@RequestMapping("/users")
@AllArgsConstructor
@Log
public class UsersController {
    private final UsersService service;

    @PostMapping("/save")
    public UserDTO saveUser(@RequestBody UserDTO dto) {
        log.info("Users :: handling :: save :: " + dto);
        try {
            return service.saveUser(dto);
        } catch (ValidationException e) {
            log.info("Error :: " + e.getMessage());
            throw new ResponseStatusException(HttpStatus.CONFLICT, e.getMessage(), e);
        } catch (NullParamException | NullDTOException | InvalidIDException e) {
            log.info("Error :: " + e.getMessage());
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, e.getMessage(), e);
        }
    }

    @GetMapping("/findAll")
    public List<UserDTO> findAllUsers() {
        log.info("Users :: handling :: find all");
        return service.findAll();
    }

    @GetMapping("/findByLogin")
    public UserDTO findByLogin(@RequestParam String login) {
        log.info("Users :: handling :: find by login :: " + login);
        return service.findByLogin(login);
    }

    @DeleteMapping("/delete/{id}")
    public ResponseEntity<Void> deleteUser(@PathVariable Integer id) {
        log.info("Users :: handling :: delete :: " + id);
        service.deleteUser(id);
        return ResponseEntity.ok().build();
    }

    @GetMapping("/getCurrentUser")
    public UserMainInfoDTO getCurrentUser() {
        log.info("Requests :: handling :: get current user");
        return service.getCurrentUser();
    }
}