package ru.adservice.backend.controllers;

import lombok.AllArgsConstructor;
import lombok.extern.java.Log;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;
import ru.adservice.backend.dto.BannerDTO;
import ru.adservice.backend.exceptions.*;
import ru.adservice.backend.services.BannersService;
import java.util.List;

@RestController
@RequestMapping("/banners")
@AllArgsConstructor
@Log
public class BannersController {
    private final BannersService service;

    @PostMapping("/save")
    public BannerDTO saveBanner(@RequestBody BannerDTO dto) {
        log.info("Banner :: handling :: save " + dto);
        try {
            return service.saveBanner(dto);
        } catch (ValidationException e) {
            log.info("Error :: " + e.getMessage());
            throw new ResponseStatusException(HttpStatus.CONFLICT, e.getMessage(), e);
        } catch (NullParamException | NullDTOException | InvalidIDException e) {
            log.info("Error ::" + e.getMessage());
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, e.getMessage(), e);
        }
    }

    @GetMapping("/findAll")
    public List<BannerDTO> findAllBanners() {
        log.info("Banners :: handling :: find all");
        return service.findAll();
    }

    @GetMapping("/findByName")
    public List<BannerDTO> findByName(@RequestParam String name) {
        log.info("Banners :: handling :: find by name like " + name);
        return service.findByNameContaining(name);
    }

    @GetMapping("/findByCategory/{id}")
    public List<BannerDTO> findByCategoryId(@PathVariable Integer id) {
        log.info("Banners :: handling :: find by category id :: " + id);
        return service.findByCategory(id);
    }

    @GetMapping("/findByAuthor/{id}")
    public List<BannerDTO> findByAuthorId(@PathVariable Integer id) {
        log.info("Banners :: handling :: find by author id :: " + id);
        return service.findByAuthor(id);
    }

    @DeleteMapping("/delete/{id}")
    public ResponseEntity<Void> deleteBanner(@PathVariable Integer id) {
        log.info("Banner :: handling :: delete :: " + id);
        service.markBannerAsDeleted(id);
        return ResponseEntity.ok().build();
    }

    @DeleteMapping("/reallyDelete/{id}")
    public ResponseEntity<Void> reallyDeleteBanner(@PathVariable Integer id) throws InvalidIDException {
        log.info("Banner :: handling :: really :: delete :: " + id);
        BannerDTO dto = service.findById(id);
        service.deleteBanner(dto);
        return ResponseEntity.ok().build();
    }
}