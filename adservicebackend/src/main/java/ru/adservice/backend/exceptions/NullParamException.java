package ru.adservice.backend.exceptions;

public class NullParamException extends Exception {
    public NullParamException(String message) {
        super(message);
    }
}