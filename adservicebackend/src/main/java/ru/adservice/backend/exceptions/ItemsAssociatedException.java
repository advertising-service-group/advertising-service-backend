package ru.adservice.backend.exceptions;

public class ItemsAssociatedException extends Exception {
    public ItemsAssociatedException(String message) {
        super(message);
    }
}
