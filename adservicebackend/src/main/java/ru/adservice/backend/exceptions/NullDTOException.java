package ru.adservice.backend.exceptions;

public class NullDTOException extends Exception {
    public NullDTOException(String message) {
        super(message);
    }
}