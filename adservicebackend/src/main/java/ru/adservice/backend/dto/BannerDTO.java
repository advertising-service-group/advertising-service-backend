package ru.adservice.backend.dto;

import lombok.Data;
import java.math.BigDecimal;

@Data
public class BannerDTO {
    private Integer id;
    private String name;
    private BigDecimal price;
    private String content;
    private Boolean deleted;
    private Integer categoryId;
    private Integer authorId;
}