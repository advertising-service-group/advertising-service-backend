package ru.adservice.backend.dto;

import lombok.Data;
import java.sql.Date;

@Data
public class RequestDTO {
    private Integer id;
    private String userAgent;
    private String ipAddress;
    private Date date;
    private Integer bannerId;
}