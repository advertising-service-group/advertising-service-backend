package ru.adservice.backend.dto;

import lombok.Data;

@Data
public class CategoryDTO {
    private Integer id;
    private String name;
    private String reqName;
    private Boolean deleted;
}