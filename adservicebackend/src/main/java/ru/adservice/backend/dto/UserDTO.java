package ru.adservice.backend.dto;

import lombok.Data;

@Data
public class UserDTO {
    private Integer id;
    private String login;
    private String password;
    private Integer roleId;
}