package ru.adservice.backend.dto;

import lombok.Data;

@Data
public class UserMainInfoDTO {
    private Integer id;
    private String login;
    private Integer roleID;
    private String roleName;
}