package ru.adservice.backend.dto;

import lombok.Data;

@Data
public class RoleDTO {
    private Integer id;
    private String name;
}