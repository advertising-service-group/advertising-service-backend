package ru.adservice.backend.configs;

import lombok.AllArgsConstructor;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.context.SecurityContextHolderStrategy;
import org.springframework.web.cors.CorsConfiguration;
import ru.adservice.backend.security.jwt.JwtConfigurer;
import ru.adservice.backend.security.jwt.JwtTokenProvider;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;

@AllArgsConstructor
@Configuration
public class SecurityConfig extends WebSecurityConfigurerAdapter {
    private final JwtTokenProvider jwtTokenProvider;

    @Bean
    @Override
    public AuthenticationManager authenticationManagerBean() throws Exception {
        return super.authenticationManagerBean();
    }

    @Bean
    SecurityContextHolderStrategy securityContextHolderStrategy() {
        return SecurityContextHolder.getContextHolderStrategy();
    }

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http
                .httpBasic().disable()
                .csrf().disable()
                .cors().configurationSource(request -> new CorsConfiguration().applyPermitDefaultValues())
                .and()
                .sessionManagement().sessionCreationPolicy(SessionCreationPolicy.STATELESS)
                .and()
                .authorizeRequests()
                .antMatchers("/categories/save").hasRole("ADMIN")
                .antMatchers("/categories/delete").hasRole("ADMIN")
                .antMatchers("/categories/reallyDelete").hasRole("ADMIN")
                .antMatchers("/users/save").hasRole("ADMIN")
                .antMatchers("/users/findByLogin").hasRole("ADMIN")
                .antMatchers("/users/delete").hasRole("ADMIN")
                .antMatchers("/roles/findById").hasRole("ADMIN")
                .antMatchers("/categories/findAll").hasAnyRole("ADMIN", "ADVERTISER")
                .antMatchers("/categories/findByName").hasAnyRole("ADMIN", "ADVERTISER")
                .antMatchers("/banners/**").hasAnyRole("ADMIN", "ADVERTISER")
                .antMatchers("/users/getCurrentUser").hasAnyRole("ADMIN", "ADVERTISER")
                .antMatchers("/getAllRequests").permitAll()
                .antMatchers("/getAd").permitAll()
                .antMatchers("/users/findAll").permitAll()
                .antMatchers("/roles/findAll").permitAll()
                .antMatchers("/auth/**").permitAll()
                .antMatchers("/help").permitAll()
                .antMatchers("/").permitAll()
                .and()
                .apply(new JwtConfigurer(jwtTokenProvider));
    }
}