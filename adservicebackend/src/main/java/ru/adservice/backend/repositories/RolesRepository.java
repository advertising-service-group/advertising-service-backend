package ru.adservice.backend.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import ru.adservice.backend.entity.Role;

public interface RolesRepository extends JpaRepository<Role, Integer> {
    Role findByName(String name);
}