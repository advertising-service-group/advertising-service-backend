package ru.adservice.backend.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import ru.adservice.backend.entity.Banner;
import java.util.List;

public interface BannersRepository extends JpaRepository<Banner, Integer> {
    Banner findByName(String name);
    List<Banner> findByNameContaining(String name);
    List<Banner> findByCategoryId(Integer id);
    List<Banner> findByAuthorId(Integer id);
}