package ru.adservice.backend.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import ru.adservice.backend.entity.Category;
import java.util.List;

public interface CategoriesRepository extends JpaRepository<Category, Integer> {
    Category findByName(String name);
    List<Category> findByNameContaining(String name);
    Category findByReqName(String name);
}