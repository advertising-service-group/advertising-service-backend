package ru.adservice.backend.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import ru.adservice.backend.entity.User;

public interface UsersRepository extends JpaRepository<User, Integer> {
    User findByLogin(String login);
}