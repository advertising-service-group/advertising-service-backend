package ru.adservice.backend.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import ru.adservice.backend.entity.Request;
import java.util.List;

public interface RequestsRepository extends JpaRepository<Request, Integer> {
    List<Request> findByIpAddress(String ip);
}