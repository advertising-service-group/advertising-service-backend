package ru.adservice.backend.security.jwt;

import lombok.extern.slf4j.Slf4j;
import ru.adservice.backend.entity.*;
import org.springframework.security.core.authority.SimpleGrantedAuthority;

@Slf4j
public final class JwtUserFactory {
    public static JwtUser create(User user) {
        return new JwtUser(
                user.getId(),
                user.getLogin(),
                user.getPassword(),
                new SimpleGrantedAuthority(user.getRole().getName())
        );
    }
}