package ru.adservice.backend.security;

import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import ru.adservice.backend.converters.UsersConverter;
import ru.adservice.backend.entity.User;
import ru.adservice.backend.repositories.UsersRepository;
import ru.adservice.backend.security.jwt.*;
import org.springframework.security.core.userdetails.*;
import org.springframework.stereotype.Service;

@AllArgsConstructor
@Service
@Slf4j
public class JwtUserDetailsService implements UserDetailsService {
    private final UsersRepository repository;

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        User user = repository.findByLogin(username);

        if (user == null) {
            throw new UsernameNotFoundException("User with login: " + username + " not found");
        }

        JwtUser jwtUser = JwtUserFactory.create(user);
        log.info("IN loadUserByLogin - user with login: {} successfully loaded", username);
        return jwtUser;
    }
}