package ru.adservice.backend.security.jwt;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.extern.slf4j.Slf4j;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import java.util.*;

@Slf4j
public class JwtUser implements UserDetails {
    private final Integer id;
    private final String login;
    private final String password;
    private final GrantedAuthority authority;

    public JwtUser(
            Integer id,
            String login,
            String password,
            GrantedAuthority authority) {
        this.id = id;
        this.login = login;
        this.password = password;
        this.authority = authority;
    }

    @JsonIgnore
    public Integer getId() { return id; }

    @Override
    public String getUsername() { return login; }

    @JsonIgnore
    @Override
    public boolean isAccountNonExpired() { return true; }

    @JsonIgnore
    @Override
    public boolean isAccountNonLocked() { return true; }

    @JsonIgnore
    @Override
    public boolean isCredentialsNonExpired() { return true; }

    @Override
    public boolean isEnabled() { return true; }

    @JsonIgnore
    @Override
    public String getPassword() { return password; }

    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {
        return Collections.singletonList(authority);
    }
}