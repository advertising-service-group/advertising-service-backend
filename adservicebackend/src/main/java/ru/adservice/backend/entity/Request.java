package ru.adservice.backend.entity;

import javax.persistence.*;
import lombok.*;
import org.hibernate.annotations.Type;
import java.sql.Date;

@Entity
@Table(name = "requests")
@Data
@NoArgsConstructor
public class Request {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @Type(type = "text")
    @Column(nullable = false)
    private String userAgent;

    @Column(nullable = false)
    private String ipAddress;

    @Column(nullable = false)
    private Date date;

    @ManyToOne
    private Banner banner;
}