package ru.adservice.backend.entity;

import javax.persistence.*;
import lombok.*;
import java.util.List;

@Entity
@Table(name = "categories")
@Data
@NoArgsConstructor
public class Category {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @Column(nullable = false)
    private String name;

    @Column(nullable = false)
    private String reqName;

    @Column(columnDefinition = "BIT(1)")
    private Boolean deleted;

    @OneToMany(mappedBy = "category")
    private List<Banner> banner;
}