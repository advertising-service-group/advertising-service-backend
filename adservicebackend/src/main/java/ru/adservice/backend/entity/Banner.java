package ru.adservice.backend.entity;

import javax.persistence.*;
import org.hibernate.annotations.Type;
import lombok.*;
import java.math.BigDecimal;
import java.util.List;

@Entity
@Table(name = "banners")
@Data
@NoArgsConstructor
public class Banner {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @Column(nullable = false)
    private String name;

    @Column(precision = 8, scale = 2, nullable = false)
    private BigDecimal price;

    @Type(type = "text")
    @Column
    private String content;

    @Column(columnDefinition = "BIT(1)")
    private Boolean deleted;

    @ManyToOne
    private Category category;

    @ManyToOne
    private User author;

    @OneToMany(mappedBy = "banner")
    private List<Request> requests;
}