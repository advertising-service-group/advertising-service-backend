package ru.adservice.backend.converters;

import lombok.AllArgsConstructor;
import org.springframework.stereotype.Component;
import ru.adservice.backend.dto.RoleDTO;
import ru.adservice.backend.entity.Role;

@AllArgsConstructor
@Component
public class RolesConverter {
    public Role dtoToRole(RoleDTO dto) {
        if (dto == null) {
            return null;
        }
        Role role = new Role();
        role.setId(dto.getId());
        role.setName(dto.getName());
        return role;
    }

    public RoleDTO roleToDTO(Role role) {
        if (role == null) {
            return null;
        }
        RoleDTO dto = new RoleDTO();
        dto.setId(role.getId());
        dto.setName(role.getName());
        return dto;
    }
}