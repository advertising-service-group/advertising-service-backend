package ru.adservice.backend.converters;

import lombok.AllArgsConstructor;
import org.springframework.stereotype.Component;
import ru.adservice.backend.dto.CategoryDTO;
import ru.adservice.backend.entity.Category;

@AllArgsConstructor
@Component
public class CategoriesConverter {
    public Category dtoToCategory(CategoryDTO dto) {
        if (dto == null) {
            return null;
        }

        Category category = new Category();
        category.setId(dto.getId());
        category.setName(dto.getName());
        category.setReqName(dto.getReqName());
        category.setDeleted(dto.getDeleted());

        return category;
    }

    public CategoryDTO categoryToDTO(Category category) {
        if (category == null) {
            return null;
        }

        CategoryDTO dto = new CategoryDTO();
        dto.setId(category.getId());
        dto.setName(category.getName());
        dto.setReqName(category.getReqName());
        dto.setDeleted(category.getDeleted());

        return dto;
    }
}
