package ru.adservice.backend.converters;

import lombok.AllArgsConstructor;
import org.springframework.stereotype.Component;
import ru.adservice.backend.dto.UserDTO;
import ru.adservice.backend.entity.*;
import ru.adservice.backend.exceptions.InvalidIDException;
import ru.adservice.backend.repositories.RolesRepository;
import java.util.Optional;

@AllArgsConstructor
@Component
public class UsersConverter {
    private final RolesRepository rolesRepository;

    public User dtoToUser(UserDTO dto) throws InvalidIDException {
        if (dto == null) {
            return null;
        }
        Optional<Role> role = rolesRepository.findById(dto.getRoleId());
        if (role.isPresent()) {
            User user = new User();
            user.setId(dto.getId());
            user.setLogin(dto.getLogin());
            user.setPassword(dto.getPassword());
            user.setRole(role.get());
            return user;
        } else {
            throw new InvalidIDException("Role with id " + dto.getRoleId() + " not found");
        }
    }

    public UserDTO userToDTO(User user) {
        if (user == null) {
            return null;
        }
        UserDTO dto = new UserDTO();
        dto.setId(user.getId());
        dto.setLogin(user.getLogin());
        dto.setPassword(user.getPassword());
        dto.setRoleId(user.getRole().getId());
        return dto;
    }
}