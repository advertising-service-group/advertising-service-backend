package ru.adservice.backend.converters;

import lombok.AllArgsConstructor;
import org.springframework.stereotype.Component;
import ru.adservice.backend.dto.BannerDTO;
import ru.adservice.backend.entity.*;
import ru.adservice.backend.exceptions.InvalidIDException;
import ru.adservice.backend.repositories.*;
import java.util.Optional;

@AllArgsConstructor
@Component
public class BannersConverter {
    private final CategoriesRepository categoriesRepository;
    private final UsersRepository usersRepository;

    public Banner dtoToBanner(BannerDTO dto) throws InvalidIDException {
        if (dto == null) {
            return null;
        }

        Optional<Category> category = categoriesRepository.findById(dto.getCategoryId());
        if (!category.isPresent()) {
            throw new InvalidIDException("Category with id '" + dto.getCategoryId() + "' not found");
        }

        Optional<User> user = usersRepository.findById(dto.getAuthorId());
        if (!user.isPresent()) {
            throw new InvalidIDException("User with id '" + dto.getAuthorId() + "' not found");
        }

        Banner banner = new Banner();
        banner.setId(dto.getId());
        banner.setName(dto.getName());
        banner.setPrice(dto.getPrice());
        banner.setContent(dto.getContent());
        banner.setDeleted(dto.getDeleted());
        banner.setCategory(category.get());
        banner.setAuthor(user.get());

        return banner;
    }

    public BannerDTO bannerToDTO(Banner banner) {
        if (banner == null) {
            return null;
        }

        BannerDTO dto = new BannerDTO();
        dto.setId(banner.getId());
        dto.setName(banner.getName());
        dto.setPrice(banner.getPrice());
        dto.setContent(banner.getContent());
        dto.setDeleted(banner.getDeleted());
        dto.setCategoryId(banner.getCategory().getId());
        dto.setAuthorId(banner.getAuthor().getId());

        return dto;
    }
}