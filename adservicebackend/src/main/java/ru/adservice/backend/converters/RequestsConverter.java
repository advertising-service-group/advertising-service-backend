package ru.adservice.backend.converters;

import lombok.AllArgsConstructor;
import org.springframework.stereotype.Component;
import ru.adservice.backend.dto.RequestDTO;
import ru.adservice.backend.entity.*;
import ru.adservice.backend.exceptions.InvalidIDException;
import ru.adservice.backend.repositories.BannersRepository;
import java.util.Optional;

@AllArgsConstructor
@Component
public class RequestsConverter {
    private final BannersRepository bannersRepository;

    public Request dtoToRequest(RequestDTO dto) throws InvalidIDException {
        if (dto == null) {
            return null;
        }
        Optional<Banner> banner = bannersRepository.findById(dto.getBannerId());
        if (!banner.isPresent()) {
            throw new InvalidIDException("Banner with id " + dto.getBannerId() + " not found");
        }
        Request request = new Request();
        request.setId(dto.getId());
        request.setBanner(banner.get());
        request.setUserAgent(dto.getUserAgent());
        request.setIpAddress(dto.getIpAddress());
        request.setDate(dto.getDate());
        return request;
    }

    public RequestDTO requestToDTO(Request request) {
        if (request == null) {
            return null;
        }
        RequestDTO dto = new RequestDTO();
        dto.setId(request.getId());
        dto.setBannerId(request.getBanner().getId());
        dto.setUserAgent(request.getUserAgent());
        dto.setIpAddress(request.getIpAddress());
        dto.setDate(request.getDate());
        return dto;
    }
}