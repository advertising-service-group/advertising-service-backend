create table roles(
    id serial primary key,
    name varchar(255)
);

create table users(
    id serial primary key,
    login varchar(255),
    password varchar(255),
    role_id int,
    foreign key (role_id) references roles (id)
);

create table categories(
    id serial primary key,
    name varchar(255),
    req_name varchar(255),
    deleted boolean
);

create table banners(
    id serial primary key,
    name varchar(255),
    price decimal(8,2),
    content text,
    deleted boolean,
    category_id int,
    author_id int,
    foreign key (category_id) references categories(id),
    foreign key (author_id) references users(id)
);

create table requests(
    id serial primary key,
    user_agent text,
    ip_address varchar(255),
    date date,
    banner_id int, foreign key (banner_id) references banners(id)
);